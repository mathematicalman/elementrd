#include "DoublyLinkedList.h"
#include <stddef.h>

DoublyLinkedList::DoublyLinkedList() {
    this->firstNode = NULL;
    this->lastNode = NULL;
}

DoublyLinkedList::~DoublyLinkedList() {
    INode 
            *currentLink = this->firstNode, 
            *nextLink = NULL;
    while(currentLink != NULL){
        nextLink = currentLink->getNext();
        delete currentLink;
        currentLink = nextLink;
    }
}

void DoublyLinkedList::addNode(INode* node){
    if(this->firstNode == NULL) {
        this->firstNode = node;
        this->lastNode = node;
        return;
    }
    this->lastNode->setNext(node);
    node->setPrev(this->lastNode);
    this->lastNode = node;
}

void DoublyLinkedList::add(int value){
    IntNode* node = new IntNode(value);
    this->addNode(node);
}

void DoublyLinkedList::add(double value){
    DoubleNode* node = new DoubleNode(value);
    this->addNode(node);
}

void DoublyLinkedList::add(std::string value){
    StringNode* node = new StringNode(value);
    this->addNode(node);
}

void DoublyLinkedList::print() const {
    INode* currentNode = this->firstNode;    
    while(currentNode != NULL){
        currentNode->print();
        currentNode = currentNode->getNext();
    }
}