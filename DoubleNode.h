#ifndef DOUBLENODE_H
#define	DOUBLENODE_H

#include "INode.h"

class DoubleNode : public INode{
public:
    double value;
    DoubleNode(const double v);
    virtual ~DoubleNode();
    virtual void print() const;
protected:        
private:
};

#endif	/* DOUBLENODE_H */

