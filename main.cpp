#include <cstdlib>
#include "IntNode.h"
#include "DoubleNode.h"
#include "StringNode.h"
#include "DoublyLinkedList.h"

using namespace std;

int main(int argc, char** argv) {
    IntNode 
        a = IntNode(1),
        b(a),//copy constructor
        c = IntNode(3);
    DoubleNode 
        d = DoubleNode(4.5),
        e = d,//operator =
        f = DoubleNode(6.5);
    StringNode 
        g = StringNode("g"),
        h = StringNode("h"),
        i = StringNode("i");
    //Display objects
    a.print();
    b.print();
    c.print();
    d.print();
    e.print();
    f.print();
    g.print();
    h.print();
    i.print();
    
    //DoublyLinkedList
    DoublyLinkedList l;
    int 
            x1 = 1,
            x2 = 2;
    l.add(x1);
    l.add(x2);
    l.add(3);
    l.add(4.5);
    l.add(5.5);
    l.add(6.5);
    l.add("g");
    l.add("h");
    l.add("i");
    
    //Display DoublyLinkedList
    l.print();
    return 0;
}