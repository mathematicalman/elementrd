#include "StringNode.h"
#include <iostream>

using namespace std;

StringNode::StringNode(const string v) : INode() {
    this->value = v;
}

StringNode::~StringNode() {
}

void StringNode::print() const {
    cout << "StringNode value: " << this->value << endl;
}