#ifndef STRINGNODE_H
#define	STRINGNODE_H

#include "INode.h"
#include <string>

class StringNode : public INode  {
public:
    std::string value;
    StringNode(const std::string v);
    virtual ~StringNode();
    virtual void print() const;
protected:        
private:
};

#endif	/* STRINGNODE_H */

