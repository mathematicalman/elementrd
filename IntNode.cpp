#include "IntNode.h"
#include <iostream>

using namespace std;

IntNode::IntNode(const int v) : INode() {
    this->value = v;
}

IntNode::~IntNode() {
}

void IntNode::print() const {
    cout << "IntNode value: " << this->value << endl;
}