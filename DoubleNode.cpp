#include "DoubleNode.h"
#include <iostream>

using namespace std;

DoubleNode::DoubleNode(const double v) : INode() {
    this->value = v;
}

DoubleNode::~DoubleNode() {
}

void DoubleNode::print() const {
    cout << "DoubleNode value: " << this->value << endl;
}