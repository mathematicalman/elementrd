#ifndef DOUBLYLINKEDLIST_H
#define	DOUBLYLINKEDLIST_H

#include "INode.h"
#include "IntNode.h"
#include "DoubleNode.h"
#include "StringNode.h"

class DoublyLinkedList {
public:
    DoublyLinkedList();
    virtual ~DoublyLinkedList(); 
    void add(int value);
    void add(double value);
    void add(std::string value);
    void print() const;
private:
    INode 
            *firstNode,
            *lastNode;//for fast adding elements
    void addNode(INode* node);
};

#endif	/* DOUBLYLINKEDLIST_H */