#ifndef INODE_H
#define	INODE_H

class INode{
public: 
    INode();
    INode(const INode& src);//copy ctor
    virtual ~INode();
    virtual void print() const = 0;  
    void setPrev(INode *inode);
    void setNext(INode *inode);
    INode* getNext() const;
    INode& operator=(const INode& right);
protected: 
    INode 
            *prev, 
            *next;  
private:
};

#endif	/* INODE_H */