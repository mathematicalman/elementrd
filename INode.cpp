#include "INode.h"
#include <stddef.h>

INode::INode() {
    this->prev = NULL;
    this->next = NULL;
}

INode::INode(const INode& src) {
    this->prev = src.prev;
    this->next = src.next;
}

void INode::setPrev(INode *inode){
    this->prev = inode;
}

INode::~INode() {
}

void INode::setNext(INode *inode){
    this->next = inode;
}

INode* INode::getNext() const{
    return this->next;
}

INode& INode::operator=(const INode& right) {
    //check if it is a self link
    if (this == &right) {
        return *this;
    }
    this->prev = right.prev;
    this->next = right.next;
    return *this;
}