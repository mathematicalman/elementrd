#ifndef INTNODE_H
#define	INTNODE_H

#include "INode.h"

class IntNode : public INode {
public:
    int value;
    IntNode(const int v);
    virtual ~IntNode();
    virtual void print() const;
protected:        
private:
};

#endif	/* INTNODE_H */